from odoo import api, models, fields
import datetime
from dateutil.relativedelta import relativedelta
from odoo.exceptions import Warning


class Offer(models.Model):
    _name = 'estate.property.offer'
    _description = 'Real Estate property offerings'
    _order = "price desc"

    price = fields.Float('Offered price')
    status = fields.Selection(
        string='Offer Status',
        selection=[('accepted', 'Accepted'), ('refused', 'Refused')],
        copy=False
    )
    partner_id = fields.Many2one('res.partner', string='Partner', required=True)
    property_id = fields.Many2one('estate.property', string='Offered For', required=True)
    validity = fields.Integer('Validity')
    date_deadline = fields.Datetime('Deadline', compute='_calc_date_deadline')
    _sql_constraints = [
        ('check_positive_offered_price', 'CHECK (price > 0)', 'The offered price must be strictly positive')
    ]

    @api.depends("validity")
    def _calc_date_deadline(self):
        for rec in self:
            rec.date_deadline = fields.datetime.now() + relativedelta(days=rec.validity)

    # def _inverse_deadline(self):
    #     for rec in self:
    #         date_diff = rec.date_deadline - fields.datetime.now()
    #         rec.validity = date_diff.days

    def action_accept(self):
        accepted = False
        for rec in self.property_id.offer_ids:
            if rec.status == 'accepted':
                accepted = True
                break
        if accepted:
            raise Warning("Can't accept multiple offers")
        else:
            self.status = 'accepted'
            self.property_id.selling_price = self.price
            self.property_id.buyer = self.partner_id

    def action_refuse(self):
        self.status = 'refused'
        self.property_id.selling_price = False
        self.property_id.buyer = False

from odoo import models, fields, api
import datetime
from dateutil.relativedelta import relativedelta
from odoo.exceptions import Warning, UserError, ValidationError
from odoo.tools import float_compare, float_is_zero


class Property(models.Model):

    _name = "estate.property"
    _description = "Real state properties"
    _order = "id desc"

    name = fields.Char(string='Property name', required=True)
    description = fields.Text('Description')
    postcode = fields.Char('Postcode')
    date_availability = fields.Date('Available from', copy=False, default=datetime.date.today() + relativedelta(months=3))
    expected_price = fields.Float('Expected price', required=True)
    selling_price = fields.Float('Selling price', readonly=True, copy=False)
    bedrooms = fields.Integer('Bedroom', default=2)
    living_area = fields.Integer('Living area(sqm)')
    facades = fields.Integer('Facades')
    garage = fields.Boolean('Garage')
    garden = fields.Boolean('Garden')
    garden_area = fields.Integer('Garden area')
    garden_orientation = fields.Selection(
        string='Type',
        selection=[('north', 'North'), ('south', 'South'), ('east', 'East'), ('west', 'West')],
    )
    active = fields.Boolean(default=True)
    state = fields.Selection(
        string='State type',
        selection=[('new', 'New'), ('received', 'Offer Received'), ('accepted', 'Offer Accepted'), ('sold', 'Sold'), ('canceled', 'Canceled')],
        default='new',
        required=True,
        copy=False
    )
    property_type_id = fields.Many2one("estate.property.type", string="Property Type")
    buyer = fields.Many2one("res.partner", string="Buyer", copy=False)
    salesperson = fields.Many2one("res.users", string="Seller", default=lambda self: self.env.user)
    property_tag_ids = fields.Many2many("estate.property.tag", string="Property Tag")
    offer_ids = fields.One2many('estate.property.offer', 'property_id', string="Offers")
    total_area = fields.Integer('Total Area', compute='_calculate_total_area')
    best_offer = fields.Float('Best price', compute='_calculate_best_price')
    _sql_constraints = [
        ('check_positive_expected_price', 'CHECK (expected_price > 0)', 'The expected price must be strictly positive'),
    ]

    @api.depends("living_area", "garden_area")
    def _calculate_total_area(self):
        for rec in self:
            rec.total_area = rec.living_area + rec.garden_area

    @api.depends("offer_ids")
    def _calculate_best_price(self):
        for rec in self:
            best = 0
            for offer in rec.offer_ids:
                if offer.price > best:
                    best = offer.price
            rec.best_offer = best

    @api.onchange("garden")
    def _onchange_garden(self):
        if self.garden:
            self.garden_area =10
            self.garden_orientation = 'north'
        else:
            self.garden_area = 0
            self.garden_orientation = ''

    def action_sold(self):
        if self.state == 'canceled':
            raise Warning("Can't sold canceled property")
        else:
            self.state = 'sold'

    def action_cancel(self):
        if self.state == 'sold':
            raise Warning("Can't cancel a sold property")
        else:
            self.state = 'sold'

    @api.constrains('selling_price', 'expected_price')
    def _check_selling_price(self):
        for rec in self:
            if (
                    not float_is_zero(rec.selling_price, precision_rounding=0.01)
                    and float_compare(rec.selling_price, rec.expected_price * 90.0 / 100.0,
                                      precision_rounding=0.01) < 0
            ):
                raise Warning(
                    "The selling price must be at least 90% of the expected price! "
                    + "You must reduce the expected price if you want to accept this offer."
                    + "the offering price is " + str(rec.selling_price) + ", which is less than 90 percent of "
                    + str(rec.expected_price)
                )


from odoo import models, fields


class Tag(models.Model):
    _name = 'estate.property.tag'
    _description = 'Real estate property tag'
    _order = "name"

    name = fields.Char(string='Property tag', required=True)
    _sql_constraints = [
        ('check_unique', 'unique (name)', 'The name of the property tag must be unique')
    ]
    color = fields.Integer('Color')

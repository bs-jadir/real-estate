from odoo import models, fields


class Type(models.Model):

    _name = "estate.property.type"
    _description = "Real state property types"
    _order = "sequence, name"

    name = fields.Char(string='Property type', required=True)
    sequence = fields.Integer('Sequence', default=1, help="Used to order properties. Lower is better.")

    _sql_constraints = [
        ('check_unique', 'unique (name)', 'The name of the property type must be unique')
    ]
    property_ids = fields.One2many('estate.property', 'property_type_id')

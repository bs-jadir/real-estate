from odoo import fields, models, api
from datetime import date, datetime


class ResPartner(models.Model):

    _inherit = "res.partner"

    dob = fields.Date("Partner DOB")

    @api.multi
    def _calc_date(self):
        dob = datetime.strptime(self.dob, '%Y-%m-%d').date()
        td = (date.today() - dob)
        age = dict()
        age['years'] = td.days//365
        rem = td.days - age['years']*365
        age['months'] = rem//31
        rem = rem - age['months']*31
        age['days'] = rem
        return age

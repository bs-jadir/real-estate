{
    'name': 'estate',
    'depends': ['base', 'report_xlsx'],
    'application': True,
    'data': [
        'data/security/ir.model.access.csv',
        'views/estate_property_views.xml',
        'views/estate_property_menus.xml',
        'data/security/estate_property_security.xml',
        'report/estate_report_templates.xml',
        'report/estate_report_views.xml',
        'views/res_partner_views_inherited.xml'
    ]
}